import urllib.request
import os
import matplotlib.pyplot as plt
import numpy as np
import input_output as io
import re
import heapq

base_url = "http://finance.yahoo.com/d/quotes.csv?s="
hist_url = "http://ichart.yahoo.com/table.csv?s="
curr_info = "&f=l1pp2"
hist_info = "&a=0&b=15&c=2015&d=0&e=15&f=2016&g=m&ignore=.csv"


test = "?s=GOOG&f=po"

def retrieve_data(stocks, data_type):
    """
    interacts with the yahoo finance API. Takes the list STOCKS, and
    collects data for each stock in STOCKS. Type of data / output determined
    by DATA_TYPE (historical type data or current day data). updates the
    STOCK_DICT object passed from the prompt function in the IO file

    calls API_RETRIEVE to actually obtain data

    returns a dictionary with the keys being the stock symbols
    """
    def api_retrieve(url):
        """
        takes STOCKS and retrieves the desired data points denoted by
        CURR_INFO and HIST_INFO from the URL

        for CURR_INFO each element in the list is a string with today's last
        trade price first and then previous close for a specific stock, whole
        list has data for all of the stocks in STOCKS. (the IF clause)

        for HIST_INFO each element is the price data for a particular day, the
        whole list pertains to one stock (the ELSE clause)
        """
        with urllib.request.urlopen(url) as a_page:
            raw = str(a_page.read().decode('utf-8'))
            print(raw)
            data_pts = [x for x in re.split('\n|\s',raw) if x != '']
            print(data_pts)

        return data_pts
        """

        plt.plot([0,1], data_pts, 'ro')
        plt.ylabel('stock price')
        plt.axis([0,4,700,800])
        ticks = np.arange(0,4,2)
        plt.xticks(ticks)
        plt.show()
        """
    stock_dict = {}

    if data_type == io.curr:
        stock_string = ','.join(stocks)
        new_url = base_url + stock_string + curr_info
        print(new_url)
        stock_data = api_retrieve(new_url)
        count = 0
        for i in stock_data:
            stock_dict[stocks[count].upper()] = [x for x in i.split(',')]
            count += 1
        print(stock_dict)

    else:
        all_prices = []
        for sym in stocks:
            prices = api_retrieve(hist_url+sym+hist_info)[2:]
            stock_dict[sym.upper()] = [float(x.split(',')[6]) for x in prices]
            for _ in stock_dict[sym.upper()]:
                all_prices.append(_)
        stock_dict[io.min_max] = (heapq.nsmallest(1,all_prices)[0], heapq.nlargest(1,all_prices)[0])        #Putting in an extra value in the stock dictionary with key min_max. Tuple with 1st element being the minimum price of all stocks and 2nd element being the max
        #print(stock_dict)



    return stock_dict



#retrieve_data(["goog","aapl","FB"], 1)

upload_type = input("Input desired stocks via text files upload or through"+ \
    " command prompt? (File/Type): ")
data_type = int(input("Are you looking for current day data in a table or " + \
    "historical trend data in a graph? Type 1 or 2, respectively: "))

if data_type !=2:
    output_type = int(input("Do you want a text file or in command line? Type 1 or 2, respectively: "))

stocks = io.prompt(io.path, upload_type, data_type)
result = retrieve_data(stocks, data_type)

if data_type == 1:
    io.output(result,data_type,output_type)
else:
    io.output(result,data_type)
