import os
from tabulate import tabulate
import numpy as np
import heapq
import matplotlib.pyplot as plt
import matplotlib.dates as plt_dates
from itertools import cycle
import datetime

path = os.getcwd()
curr = 1                                                                        # used to check what type of output user entered
txt = 1                                                                         # used to check what type of output user wants
min_max = 'MIN_MAX'                                                             # used as a key in the stock dictionary populated in "run_stock.py". This key points to a tuple with the min and max stock prices of requested stocks
years = [2015,2015,2015,2015,2015,2015,2015,2015,2015,2015,2015,2015,2016]
months = [1,2,3,4,5,6,7,8,9,10,11,12,1]
days = [15]*13
dates = [datetime.datetime(x,y,z) for (x,y,z) in zip(years, months, days)]

def pop_dict(lst, dct):
    """
    function that populates a dictionary using iterable INPUT variable as its
    keys. Returns nothing, just mutates the DCT object (dictionary)
    """
    for item in lst:
        if item not in dct:
            dct[item.upper()] = []

def prompt(path, upload_type, data_type):
    """
    function used to run the input code to prompt the user for the desired
    stock ticker symbols. Returns a list of stocks
    """
    def file_upload(f):
        """
        Take a .txt file and take all listed stock ticker symbols and load
        into a list object. mutates the list object (LST)
        """
        all_stocks = []
        with open(f, mode='r') as txt_file:
            for line in txt_file:
                stocks = line.split()
                for item in stocks:
                    all_stocks.append(item)
        return all_stocks

    if upload_type.lower() == 'file':
        file_name = input("Type in input file name (w/extension)")
        f = os.path.join(path, file_name)
        stocks = file_upload(f)
    else:
        _ = input("Type stock symbols with space in between each one: ")
        stocks = _.split()

    return stocks

def output(data, data_type, output_type=None):
    """
    prompts user on type of output they want..command prompt vs. file then executes
    """
    if data_type == curr:
        headers = ["Company", "Today's Close", "Yesterday Close", "% Change"]
        if output_type == txt:
            with open(os.path.join(path,"stocks_output.txt"), mode='w') as output_file:
                output_file.write(''.join(col.rjust(30) for col in headers))
                output_file.write('\n')
                for pt in data:
                    output_file.write(pt.rjust(30))
                    output_file.write(''.join(str(num).rjust(30) for num in data[pt]))
                    output_file.write('\n')

        else:
            for key in data.keys():
                data[key].insert(0,key)
            data_print = [data[pt] for pt in data]
            print(tabulate(data_print, headers, tablefmt='psql'))
    else:
        price_range = data.pop(min_max)
        #price_range_graph = np.arange(min(price_range[0] - 10,0), price_range[1], (price_range[0] - price_range[1]) / 6)
        plt_dates.date2num(dates)
        #print(type(dates[0]))
        lines = ["r:","g-.","b--","g-","y:"]                                    # matplotlib labels for different style of lines on the line graph
        linecycler = cycle(lines)
        #plt.xlim((date_range[0], date_range[-1]))
        plt.ylim(max((price_range[0]-10,0)), (price_range[1]+10))
        plt.ylabel("Stock Price")
        plt.xlabel("Day")

        for sym in data:
            print(np.asarray(dates))
            print(np.asarray(data[sym][::-1]))
            plt.plot_date(np.asarray(dates), np.asarray(data[sym][::-1]),\
                next(linecycler), xdate=True, label=sym, linewidth=4.0)
            plt.legend()
        plt.show()



#print(prompt(path))


