Purpose:
Simple practice working with an API, specifically Yahoo Finance API. Also, got
to practice using matplotlib module and outputing graphs and simple tables
in the command prompt.

How to run:
Follow the prompts given in the command line.
If want to input via text file. Just put the stock ticker symbols in a text
file in the same folder as "run_stock.py". Ticker symbols should be seperated
via spaces and/or newlines.

Requirements:
Needs to matplotlib installed in order to output the line graph.
